#!/bin/bash

for i in *.svg; do rsvg-convert -f pdf $i -o "${i%.*}".pdf; done
for i in *.svg; do rsvg-convert -f png $i -o "${i%.*}".png; done

