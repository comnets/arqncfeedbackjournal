# Simple makefile that use Rubber to compile latex code

MAIN = main

TEX = rubber -W refs -W misc -d

# PDF reader
PDFREADER=evince
SYNCTEX_PDFREADER=evince-sync

all: $(MAIN).pdf

$(MAIN).pdf: $(SRCS)
	@echo ""
	$(TEX) $(MAIN).tex
	@echo ""
	@echo "** Done compiling $(MAIN).pdf"
	@echo ""

force: clean all

clean:
	rm -f $(MAIN).pdf *.toc *.aux *.ps *.log *.lof *.bbl *.blg *.dvi */*.aux *.glo *.gls *.ilg *.nls *.nlo *.tex~ */*.tex~ */*.tex.backup */*/*.tex~ */*/*/*.tex~ *.ist *.alg *.acn *.acr *.out *.glg *.lox *.synctex.gz

open: $(MAIN).pdf
	@echo ""
	@echo "Opening $(MAIN).pdf"
	screen -d -m ${PDFREADER} $(MAIN).pdf

sopen: $(MAIN).pdf
	@echo ""
	@echo "Opening $(MAIN).pdf"
	screen -d -m ${SYNCTEX_PDFREADER} $(MAIN).pdf
	./myeditor $(MAIN).pdf 1 1
